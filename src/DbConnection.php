<?php

namespace GrotTheCheat\MySqlConnector;

use Composer\Autoload\ClassLoader;
use GrotTheCheat\YamlConfig\YamlConfig;

final class DbConnection extends \PDO
{
    public function __construct()
    {
        $config_path = $this->getApplicationRootDir();
        $config = YamlConfig::load($config_path);

        $dsn = sprintf(
            'mysql:host=%s:%s;dbname=%s',
            $config->get("grotthecheat.mysql-connector.server"),
            $config->get("grotthecheat.mysql-connector.port"),
            $config->get("grotthecheat.mysql-connector.schema")
        );

        parent::__construct(
            $dsn,
            $config->get("grotthecheat.mysql-connector.username"),
            $config->get("grotthecheat.mysql-connector.password")
        );
    }

    private function getApplicationRootDir(): string
    {
        $reflection = new \ReflectionClass(ClassLoader::class);
        $vendor_dir = preg_replace('/^(.*)\/composer\/ClassLoader\.php$/', '$1', $reflection->getFileName() );
        $vendor_dir .= '/../application-settings.yaml';
        return $vendor_dir;
    }
}